
from django.shortcuts import redirect
from rest_framework import viewsets
from .serializers import PostSerializer
from .models import Post 



# Create your views here.


class PostView(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

def delete(request, delete_id):
    ref = Post.objects.filter(delete=delete_id).delete()
    print(ref)

    return redirect("http://localhost:3000/deletesuccess")